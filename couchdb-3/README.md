*(brouillon, incomplet)*

* [Tutoriels CouchDB](/tutoriels-couchdb)
* [Tutoriels CouchDB (notes)](/tutoriels-couchdb-notes)

**CouchDB** est un serveur de base de données (documents JSON) avec une interface HTTP.

À la fin de ce tutoriel, vous afficherez une sélection de documents dans une page html complète sur votre serveur **CouchDB** en mode local. Seul la commande **curl** et un fureteur sont requis, au delà de l'installation.

## Installer **CouchDB**
Consultez le premier tutoriel sur CouchDB pour l'installation et la création de votre première base de données.


## Les index secondaires (les *views*)

Bienvenue au monde de *map/reduce*. N'ayez pas peur, ça ne mord pas. On a vu _all_docs dans le tutoriel #2, c'était l'index principal. Maintenant on va découvrir les *views*, ou les index secondaires.

On se souviendra que le résultat d'un *view* est un ensemble de rangées avec trois ou quatre clés: ```id```, ```key```, ```value``` et accessoirement ```doc``` si on a passé l'argument "include_docs". Dans le cas de l'index principal, les valeurs de ```id``` et ```key``` sont identiques tandis que les views définissent les index (les ```key```) et chacune des 3-4 clés des rangées.

Chaque view définit une fonction map et optionnellement une fonction reduce. On verra les fonctions reduce dans un prochain tutoriel, pour l'instant on va créer la première fonction map pour le premier view.

### La fonction map d'un view
Les fonctions map prennent toutes un argument doc. Quand il y a réindexation (parce qu'on a modifié un document, la fonction map, etc.), chaque document concerné est passé comme argument à toutes les fonctions map.

Une fonction map peut émettre plus d'un résultat par doc reçu en argument ou ne rien émettre du tout.

#### Fonction **CouchDB** emit(key, value)

L'index principal _all_docs peut être généré avec cette fonction JavaScript:

```javascript
function (doc) {
  emit(doc._id, {rev: doc._rev});
}
```

Notez qu'on peut émettre ce que l'on veut:

```javascript
function (doc) {
  emit(doc._id, doc.titre);
}
```

Dans cet exemple, la valeur émise est une chaine tandis que dans l'exemple précédent, c'est un object avec une seule clé ```rev```.

Et si on était intéressant seulement aux documents avec une clé ```titre``` et ignorer les autres?

```javascript
function (doc) {
  if (!doc.titre) { return; }
  emit(doc._id, doc.titre);
}
```

On aurait pu aussi écrire:

```javascript
function (doc) {
  if (doc.titre) {
    emit(doc._id, doc.titre);
  }
}
```

Mais pour les fonctions plus longues (habituellement), c'est préférable de vite filtrer (avec un ```return```) les documents qui ne réponde pas aux critères. Ça évite du coup un brin d'indentation.

Enfin, rappelez-vous de la clé ```tags``` utilisées dans les documents des tutoriels précédents. Comme il s'agit de tableaux, on pourait faire quelque chose comme:

```javascript
function (doc) {
  if (!isArray(doc.tags) || !doc.tags.length) { return; }
  doc.tags.forEach(function (tag) {
    emit(tag.toLowerCase());
  }
}
```

Notez qu'on n'a pas besoin de passer un deuxième argument à ```emit()```, c'est ```null``` qui sera émit comme valeur dans ce cas. Remarquez aussi qu'au lieu de passer doc._id comme premier argument à emit (key), on passe tag, c'est à dire un élément de doc.tags à la fois via la méthode ```forEach()```. Pour les documents qui ont deux tags, on émettra deux fois (une fois pour chaque tag). Pour illustration, on met le tag en minuscules. Enfin, si le document n'a pas de champ doc.tags, que celui-ci n'est pas un tableau ou encore s'il est vide, rien ne sera émit pour ce document.

### Plusieurs views
Si on veut ordonner et regrouper les documents par un champ de date, comme ```dateheure```, on va créer un view. Grouper par titre? Un autre view avec où on ```emit(doc.titre)```. Et un autre view pour grouper par un champ auteur, etc.

## Prochaines étapes
*(copié du tuto #1)*

La première chose à faire serait de créer un autre document avec les champs titres et tags et le ID de votre choix pour le convertir en HTML à son tour et voir que tout fonctionne comme promis. Appelons ça un exercise du lecteur. Sentez-vous bien à l'aise d'adapter la fonction page, peut-être ajouter un champ ```contenu``` aux documents et l'afficher en plus du titre?

Vous avez maintenant un serveur web (HTTP/HTML) et une méthode de base mais en même temps universelle pour créer, mettre à jour et effacer des documents. Vous savez aussi comment faire une fonction pour convertir un document JSON en HTML.

Dans le prochain tutoriel, vous apprendrez comment faire une liste de documents dans l'ordre de votre choix et aussi comment l'afficher sous forme de HTML. Les autres tutoriels couvriront les utilisateurs (et les rôles), la sécurité (les accès), des URL plus jolis (rewrites), les fichiers attachés aux documents, comment faire un serveur web public, la réplication, les fils de changements, les outils de développement et bien plus.
