*(brouillon, incomplet)*

**CouchDB** est un serveur de BD (base de données de documents JSON) NoSQL avec une interface HTTP.

* [Tutoriels CouchDB](/tutoriels-couchdb) (ce document)
* [Tutoriels CouchDB (notes)](/tutoriels-couchdb-notes)


## Expliquer le markup

* ```clé``` / ```champ```
* *anglais*
* **Nom** et **commande**
* "nom de BD", "nom de document" et "nom de fichier"

Dans les exemples la commande à taper tient sur une seule ligne. Le «$ » en début de ligne indique le *prompt*, on ne doit pas le taper. Les signes suivantes représentent la sortie de la commande.

## Structure commune aux tutoriels
* Ce que vous apprendrez
* Prérequis
* Outils nécessaires
* Liens/références
* Durée approximative

## Les tutoriels

### [Tutoriel #1: Générer page web à partir d'un document JSON](/tutoriel-couchdb-1)
À la fin de ce tutoriel, vous afficherez une page HTML complète sur votre serveur **CouchDB** en mode local. Seul la commande **curl** et un fureteur sont requis, au delà de l'installation.

### [Tutoriel #2: Générer page web à partir de tous les documents JSON](/tutoriel-couchdb-2)
À la fin de ce tutoriel, vous afficherez de l'information sur l'ensemble des documents dans une page HTML complète sur votre serveur **CouchDB** en mode local. Seul la commande **curl** et un fureteur sont requis, au delà de l'installation.

### [Tutoriel #3: Générer page web à partir d'une sélection de documents JSON](/tutoriel-couchdb-3)
À la fin de ce tutoriel, vous afficherez une sélection de documents dans une page HTML complète sur votre serveur **CouchDB** en mode local. Seul la commande **curl** et un fureteur sont requis, au delà de l'installation.

### Tutoriel #4: Page web AJAX pour éditer un document

### Tutoriel #5: Page web (sans JavaScript) pour éditer un document

### Tutoriel #6: Introduction à Futon (le PhpMyAdmin natif à CouchDB)

### Tutoriel #n: ...

### Tutoriel #n: CouchDB en production comme serveur web applicatif


