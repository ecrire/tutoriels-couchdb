*(brouillon, incomplet)*

* [Tutoriels CouchDB](/tutoriels-couchdb)
* [Tutoriels CouchDB (notes)](/tutoriels-couchdb-notes) (ce document)

## Prochains tutoriels
* [Tutoriel #1: Générer page web à partir d'un document JSON](/tutoriel-couchdb-1)
* [Tutoriel #2: Générer page web à partir de tous les documents JSON](/tutoriel-couchdb-2)
* [Tutoriel #3: Générer page web à partir d'une sélection de documents JSON](/tutoriel-couchdb-3)
* Tutoriel #4: Page web AJAX pour éditer un document
* Tutoriel #5: Page web (sans JavaScript) pour éditer un document
* Tutoriel #6: Introduction à Futon (le PhpMyAdmin natif à CouchDB)
* Tutoriel #n: ...
* Tutoriel #n: ...
* Tutoriel #n: ...
* Tutoriel #n: CouchDB en production comme serveur web applicatif

## Références
* <http://robin.millette.info/tutoriels-couchdb>
* <https://gitlab.com/ecrire/tutoriels-couchdb>
* <http://docs.couchdb.org/>
* <http://couchdb.apache.org/>
* <http://www.replication.io/>

## Matériel à couvrir
* views/lib/librairie-1.js
* lodash
* ajax (fonction w3c fetch)
* pouchdb
* offline first
* vue.js
* riot.js
* couchapps
* changes feed
* filtres
* replication
* daemons (process control)
* compaction
* futon
* rewrites/vhost
* update functions
* iptables 80 -> 5984
* proxy ngingx, site quasi-statique
* local.ini (127.0.0.1 vs 0.0.0.0)
* admin user
* general users
* _security
* update_validate function
* outils: erica, couchdb-bootstrap, etc.
* babel (es2015)
* gulp, yeoman, npm, bower
* ssl
* lucene/solr
* elasticsearch/logstash
* geo
* json schemas
* hébergement: cloudant, iriscouch, smileupps, etc.
